﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform;
using Tao.DevIl;


namespace Sphera
{
    public partial class Teapot : Form
    {
        public Teapot()
        {
            InitializeComponent();
            holst.InitializeContexts();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Glut.glutInit(); // инициализация Glut 
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
         
            Gl.glEnable(Gl.GL_DEPTH_TEST); // подключаем буфер глубины
            Gl.glEnable(Gl.GL_ALPHA_TEST);// тестирование альфа-канала
            Gl.glEnable(Gl.GL_BLEND); // смешивание цветов
            Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);//задаем арифметику пикселей
            // настройка проекции 
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(45, (float)holst.Width / (float)holst.Height, 0.1, 200);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // отчитка окна 
            Gl.glClearColor(255, 255, 255, 1);
            // установка порта вывода в соответствии с размерами holst 
            Gl.glViewport(0, 0, holst.Width, holst.Height);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            Gl.glLoadIdentity(); // загружаем единичную матрицу
            Gl.glColor3f(0, 0.5f, 0.9f); // цвет чайника

            Gl.glPushMatrix(); 
            Gl.glTranslated(0, 0, -7);// сдвигаем чайник в область видимости по оси z
            Gl.glRotated(135, 1, 1, 0);// поворачиваем чайник
            Glut.glutSolidTeapot(1.5); // рисуем непрозрачный чайник 
            Gl.glPopMatrix();

            Gl.glPushMatrix();
            Gl.glTranslated(0.8f, 0, -5); 
            Gl.glColor4f(1, 0, 0.3f, 0.5f); // задаем прозрачность
            Glut.glutSolidTorus(0.5, 1.3, 20, 20); // рисуем полупрозрачный тор
            Gl.glPopMatrix();

            holst.Invalidate();
        }
    }
}
